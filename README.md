# Planck Valo
This is a VGA chip for homebrew computers. It is easiest to integrate it into a 6502 based system but should also be usable with other processors. It is based on the Gowin GW1N-1 FPGA with 8k of BRAM. All the BRAM is used as video RAM.
It is capable of displaying a maximum of 256 colors and a maximum resolution of 640x480 pixels in monochrome mode.
There 8Kb of video memory. To prevent taking up too much address space and bringing too many address lines to the chip, the video memory is accessed via a register indirection mechanism.

## PIN layout
```
        __________
A0   --|1       28|-- VCC
A1   --|2       27|-- R/W
A2   --|3       26|-- /IRQ
A3   --|4       25|-- /EN
/RST --|5       24|-- B0
D7   --|6       23|-- B1
D6   --|7       22|-- G0
D5   --|8       21|-- G1
D4   --|9       20|-- G2
D3   --|10      19|-- R0
D2   --|11      18|-- R1
D1   --|12      17|-- R2
D0   --|13      16|-- VSYNC
VSS  --|14      15|-- HSYNC
        ‾‾‾‾‾‾‾‾‾‾
```

- `A0` to `A3` are the 4 address lines which allow for controlling to/from which register we are reading / writing. connected to the first for address lines of the processor.
- `/RST` is the reset signal and should be connected directly to your reset circuit.
- `D0` to `D7` are the data lines, connecting to the respective pins on the processor.
- `VSS` and `VCC` are 0V and +5V respectively
- `HSYNC` and `VSYNC` are the display sync signal and should be connected to your VGA plug
- `RO` to `R2` are the red values for each pixels, they should be connected via a resistor DAC to your VGA output. 
- `GO` to `G2` are the green values for each pixels, they should be connected via a resistor DAC to your VGA output. 
- `B0` and `B1` are the blue values for each pixels, they should be connected via a resistor DAC to your VGA output. 
- `/EN` is the enable pin, active low.
- `/IRQ` is the onfigurable IRQ signal, active low
- `R/W` indicates whether we are writing to or reading from the register. Active low which means if it is low then we are writing. Can be connected straight to the CPU R/W pin

## Video modes
The software is still in flux at this point, but at this time these are the display modes
### Character modes
#### 640x480 mono
The first mode is 640x480 character based monochrome mode, with 128 built-in characters and 128 user defined characters. All characters are 8x8 pixels. the built-in characters can also be modified.

This mode allows a display of 80 characters wide by 60 characters high.
The memory contains 90x68 characters indices from $0000 to $17E7, this allows for limited screen scrolling.

The memory from $1800 to $1BFF contains the predefined characters. These can be modified.
The memory from $1C00 to $1FFF contains the user characters


#### 320x240 color
This mode allows color to be displayed but only allows 40x30 characters to be displayed on screen.
The memory contains 50x40 characters, allowing for limited scrolling.

Each characters in represented by three bytes: the first byte is the index into the characters from $1800, the second byte is the foreground color and the third byte is the background color.

The memory from $1800 to $1BFF contains the predefined characters. These can be modified.
The memory from $1C00 to $1FFF contains the user characters


### Bitmap modes
#### 320x200 monochrome
Video memory from $0000 to $1f40, one bit per pixel
Address $1FFE contains the foreground color and address $1FFF contains the background color.

#### 160x100 color
TBD

## Registers
There are 8 registers that allow for controlling video memory indirectly.

<table>
        <thead>
        <tr>
                <th>Address</th>
                <th>Name</th>
                <th>Bit 7</th>
                <th>Bit 6</th>
                <th>Bit 5</th>
                <th>Bit 4</th>
                <th>Bit 3</th>
                <th>Bit 2</th>
                <th>Bit 1</th>
                <th>Bit 0</th>
        </tr>
        </thead>
        <tbody>
        <tr>
                <td>$0</td>
                <td>CTRL_REG</td>
                <td colspan=6 align=center>INCREMENT</td>
                <td colspan=2 align=center>MODE</td>
        </tr>
        <tr>
                <td>$1</td>
                <td>ADDR_LOW_REG</td>
                <td colspan=3 align=center>-</td>
                <td colspan=5 align=center>5 low address bits</td>
        </tr>
        <tr>
                <td>$2</td>
                <td>ADDR_HIGH_REG</td>
                <td colspan=8 align=center>8 high address bits</td>
        </tr>
        <tr>
                <td>$3</td>
                <td>DATA_REG</td>
                <td colspan=8 align=center>DATA</td>
        </tr>
        <tr>
                <td>$4</td>
                <td>IEN_REG</td>
                <td>H_SYNC</td>
                <td colspan=6 align=center>-</td>
                <td>V_SYNC</td>
        </tr>
        <tr>
                <td>$5</td>
                <td>INTR_REG</td>
                <td>H_SYNC</td>
                <td colspan=6 align=center>-</td>
                <td>V_SYNC</td>
        </tr>
        <tr>
                <td>$6</td>
                <td>HSCROLL_REG</td>
                <td colspan=8 align=center>TBD</td>
        </tr>
        <tr>
                <td>$7</td>
                <td>VSCROLL_REG</td>
                <td colspan=8 align=center>TBD</td>
        </tr>
        </tbody>
</table>