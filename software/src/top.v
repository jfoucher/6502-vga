module vga_bram (
    input wire SYS_CLK,
    input wire CPU_CLK,
    input wire [2:0] register_address,
    inout wire [7:0] data_port,
    input wire CS,
    input wire RW,
    input wire reset,
    output wire h_sync,
    output wire v_sync,
    output wire irq,
    output reg [7:0] rgb,
    output reg [2:0] led,
    output wire data_en
);



`define HOR_CHARS 7'd80
`define HOR_CHARS_LORES 7'd40
`define VERT_CHARS 7'd60
`define VERT_CHARS_LORES 7'd30
`define CHAR_DATA_START 13'h0
`define FONT_DATA_START 13'h1800

`define CTRL_REG 3'd0       // Formatted as follows |INCR_5|INCR_4|INCR_3|INCR_2|INCR_1|INCR_0|MODE_1|MODE_0|  default to LORES
`define ADDR_LOW_REG 3'd1   // also contains the increment ||||ADDR4|ADDR_3|ADDR_2|ADDR_1|ADDR_0|
`define ADDR_HIGH_REG 3'd2
`define DATA_REG 3'd3
`define IEN_REG 3'd4    // formatted as follows |VSYNC| | | | | | |HSYNC|
`define INTR_REG 3'd5   // formatted as follows |VSYNC| | | | | | |HSYNC|
`define HSCROLL_REG 3'd6    // Scrolls one column in character mode and one pixel in pixel mode - negative is left, positive is right
`define VSCROLL_REG 3'd7    // Scrolls one line in character mode and one pixel in pixel mode - negative is down, positive is up

`define MODE_CHAR_LORES 2'd0
`define MODE_CHAR_HIRES 2'd1
`define MODE_PX_LORES 2'd2
`define MODE_PX_HIRES 2'd3

reg [7:0] v_scroll = 0;
reg v_scroll_up;

assign data_en = ~CS;

assign irq = 1'b1;


wire CLK_100M;
wire CLK_25M;
wire clkout_o;

Gowin_rPLL pll(
    .clkout(clkout_o), //output clkout
    .clkoutp(CLK_100M), //output clkoutp
    .clkoutd(CLK_25M), //output clkoutd
    .clkin(SYS_CLK) //input clkin
);



wire [12:0] ada_i = address;
wire [7:0] douta_o;
reg [7:0] dina_i;
wire cea = 1'b1;
reg ocea;

reg [12:0] adb_i;
wire [7:0] doutb_o;

reg [1:0] mode;

reg data_inout;             // If 1, means data_port is an output, otherwise, it's an input

assign data_port = data_inout ? data_out : 8'bzzzzzzzz;

wire enable = ~CS;
wire write = ~RW;
wire read = RW;
reg rama_wr;
reg ramb_wr;
reg [7:0] dinb;

reg [12:0] address;

Gowin_DPB bram(
    .ada(address), //input [12:0] ada
    .douta(douta_o), //output [7:0] douta
    .dina(dina_i), //input [7:0] dina
    .clka(CLK_100M), //input clka
    .ocea(enable), //input ocea
    .cea(cea), //input cea
    .reseta(~reset), //input reseta
    .wrea(rama_wr), //input wrea
    .clkb(CLK_100M), //input clkb
    .oceb(1'b1), //input oceb
    .ceb(1'b1), //input ceb
    .resetb(~reset), //input resetb
    .wreb(ramb_wr), //input wreb
    .adb(adb_i), //input [12:0] adb
    .doutb(doutb_o), //output [7:0] doutb
    .dinb(dinb) //input [7:0] dinb
);


reg [5:0] increment_reg;
wire [7:0] increment = (increment_reg == 0) ? 0 : (increment_reg <= 8 ? (1 << (increment_reg-1)) : 
    (increment_reg == 9 ? 3 : 
    (increment_reg == 10 ? 10 : 
    (increment_reg == 11 ? `HOR_CHARS_LORES : 
    (increment_reg == 12 ? `HOR_CHARS : 
    (increment_reg == 13 ? `HOR_CHARS*2 : 
    (increment_reg == 14 ? `HOR_CHARS_LORES*3 : `HOR_CHARS*3
)))))));
reg increment_neg;
reg [2:0] next;
reg [2:0] read_next;
reg [2:0] reg_address;
reg [2:0] reg_address_read;
reg [7:0] data_in;
reg [7:0] data_out;
reg [7:0] cnt;
reg [7:0] read_cnt;
reg [7:0] ram_out;
reg will_write;
reg inc_addr;



`include "input.v"

reg [10:0] line_cnt;
reg [10:0] px_cnt;

always @(posedge CLK_25M) begin
    px_cnt <= px_cnt + 1;
    if (line_cnt >= 525) begin
        line_cnt <= 0;
    end
    else if (px_cnt >= 800) begin
        px_cnt <= 0;
        line_cnt <= line_cnt + 1;
    end
end

// TODO after setting data, anything that reads OR write will write the same data again at the next address

assign h_sync = (px_cnt < 656 || px_cnt > 752);

assign v_sync = (line_cnt < 490 || line_cnt > 492);

wire vblank = ((line_cnt > 480) && (line_cnt < 524));

wire hires = (mode & 2'd1 == 2'd1);
wire color = (mode & 2'd2 == 2'd2);

`include "video.v"

wire active_video = (px_cnt < 640 && line_cnt < 480);

always @(posedge CLK_25M) begin
    if (active_video && cur_px) begin
        //led <= cur_px;
        rgb <= fgcolor;
        //rgb <= (line_cnt >> 3) & 8'hFF;
        //led <= 3'd2;
    end
    else 
    if (active_video) begin
        //led <= char_pixels;
        rgb <= bgcolor;
        //rgb <= ((px_cnt >> 1) & 8'hE0) | ((px_cnt >> 2) & 8'h1C) | ((px_cnt >> 3) & 8'h03);
        //rgb <= (line_cnt>>3) + (px_cnt >> 3);
        //led <= 3'd4;
    end
    else begin
        //led <= 3'b0;
        rgb <= 8'd0;
    end
end

`define SCROLL_STATE_READ1 3'd0
`define SCROLL_STATE_READ2 3'd1
`define SCROLL_STATE_WRITE1 3'd2
`define SCROLL_STATE_WRITE2 3'd3
`define SCROLL_STATE_RESET 3'd4

reg [3:0] scroll_state;
reg scroll_done;
reg [6:0] line;
reg [6:0] col;
reg [12:0] addr;
reg[7:0] tmp_byte;

wire [12:0] addr_read = v_scroll_up ? (`CHAR_DATA_START + (line + v_scroll) * `HOR_CHARS + col) : (`CHAR_DATA_START + (`VERT_CHARS - line - v_scroll) * `HOR_CHARS + col);

wire [12:0] addr_write = v_scroll_up ? (`CHAR_DATA_START + (line * `HOR_CHARS) + col) : (`CHAR_DATA_START + ((`VERT_CHARS - line) * `HOR_CHARS) + col);

wire do_scroll = vblank && !scroll_done && (v_scroll > 0);

always @(posedge CLK_25M) begin
    if (!do_scroll) begin
        line <= 0;
        col <= 0;
        scroll_done <= 0;
        ramb_wr <= 0;
        addr <= 0;
        scroll_state <= `SCROLL_STATE_READ1;
    end
    else begin
        case (scroll_state)
        `SCROLL_STATE_READ1:
        begin
            led <= 3'b101; // blue
            ramb_wr <= 1'b0;
            
            if ((v_scroll_up && ((line + v_scroll) < `VERT_CHARS)) || (!v_scroll_up && ((line - v_scroll) < `VERT_CHARS))) begin
                addr <= addr_read;
                scroll_state <= `SCROLL_STATE_READ2;
            end
            else begin
                addr <= 0;
                tmp_byte <= 8'h20;
                scroll_state <= `SCROLL_STATE_WRITE1;
            end
        end

        `SCROLL_STATE_READ2:
        begin
            ramb_wr <= 1'b0;
            // we are not outside the screen yet, so keep copying
            if (adb_i == addr) begin
                tmp_byte <= doutb_o;
                scroll_state <= `SCROLL_STATE_WRITE1;
            end
            else begin
                scroll_state <= `SCROLL_STATE_READ2;
            end
        end
        `SCROLL_STATE_WRITE1:
        begin
            led <= 3'b110; // green
            ramb_wr <= 1'b0;
            if ((addr_write >= `CHAR_DATA_START) && (addr_write < `FONT_DATA_START)) begin
                addr <= addr_write;
                scroll_state <= `SCROLL_STATE_WRITE2;
            end
            else begin
                addr <= 0;
                dinb <= 0;
                scroll_state <= `SCROLL_STATE_WRITE1;
            end
        end
        `SCROLL_STATE_WRITE2:
        begin
            led <= 3'b110; // green
            
            if (adb_i == addr) begin
                dinb <= tmp_byte;
                ramb_wr <= 1'b1;

                
                if (line >= `VERT_CHARS) begin
                    scroll_done <= 1'b1;
                end
                else if (col >= `HOR_CHARS) begin
                    line <= line + 1;
                    col <= 0;
                    scroll_state <= `SCROLL_STATE_READ1;
                end
                else begin
                    scroll_state <= `SCROLL_STATE_READ1;
                    col <= col + 1;
                end
            end
            else begin
                ramb_wr <= 1'b0;
                scroll_state <= `SCROLL_STATE_WRITE2;
            end
            
            //led <= 3'b110; //  green
            // After writing, increment char and/or line
        end
        default:
        begin
            led <= 3'b111;
            ramb_wr <= 0;
            addr <= 0;
            scroll_state <= `SCROLL_STATE_READ1;
        end
        endcase
    end
    
end
endmodule