//Copyright (C)2014-2021 GOWIN Semiconductor Corporation.
//All rights reserved.
//File Title: Timing Constraints file
//GOWIN Version: 1.9.6.02 Beta
//Created Time: 2021-01-17 21:22:10
create_clock -name SYS_CLK -period 41.667 -waveform {0 20.834} [get_ports {SYS_CLK}]
