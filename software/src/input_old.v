
// always @(posedge SYS_CLK) begin
//     //if (set_addr_state) begin
        
//     //end
// end


always @(posedge enable) begin   // enable is gated with PHI2
    led <= 3'b101; //blue
    //We get the address here
    if (set_addr_state) begin
        address <= tmp_addr;
    end
    
    reg_address <= register_address;
    will_write <= ~write;
    if (write) begin
        case (register_address)
        `CTRL_REG:
        begin
            data_out <= mode & 8'h03 | (increment_reg << 2) | (increment_neg << 7);
            cea <= 0;
            inc_addr <= 0;
        end
        `ADDR_LOW_REG:
        begin
            data_out <= tmp_addr[4:0];
            inc_addr <= 0;
            cea <= 0;
        end
        `ADDR_HIGH_REG:
        begin

            data_out <= tmp_addr[12:5];
            inc_addr <= 0;
            cea <= 0;
        end
        `DATA_REG:
        begin
            cea <= 1;
            //led <= douta_o;

            data_out <= douta_o;
            inc_addr <= 1;
        end
        default:
        begin
            cea <= 0;
            data_out <= 8'd0;
            inc_addr <= 0;
        end
        endcase
    end
    else begin
        inc_addr <= 0;
        case (register_address)
        `DATA_REG:
        begin
            cea <= 1;
        end
        default:
        begin
            cea <= 0;
        end
        endcase
       
    end


end

// TODO after setting data, anything that reads OR write will write the same data again at the next address

always @(posedge SYS_CLK) begin
    if (enable && will_write) begin
        data_in <= data_port;
    end
end
reg [12:0] tmp_addr;
reg set_addr_state;


always @(negedge enable) begin   // enable is gated with PHI2
    //We get/set the data here
    //led <= 3'b111; //blue
    //data_in <= data_port;
    if (will_write) begin
        case (reg_address)
        `CTRL_REG:
        begin
            rama_wr <= 0;
            mode <= data_in & 2'b11;
            increment_reg <= data_in[6:2];
            increment_neg <= data_in[7];
            set_addr_state <= 0;
        end
        `ADDR_LOW_REG:
        begin
            rama_wr <= 0;
            tmp_addr[4:0] <= data_in[4:0];
            //led <= 3'b101; //blue
            set_addr_state <= 1;
            //led <= 3'b011; //red
            //led <= 3'b010; //yellow
            //led <= 3'b001; //purple
            //led <= 3'b100; //turquoise

        end
        `ADDR_HIGH_REG:
        begin
            rama_wr <= 0;
            tmp_addr[12:5] <= data_in;
            set_addr_state <= 1;
        end
        
        `DATA_REG:
        begin
            rama_wr <= 1;
            dina_i <= data_in;
            set_addr_state <= 1;
            if (increment_neg) begin
                tmp_addr <= address - increment;
            end begin
                tmp_addr <= address + increment;
            end
        end
        default:
        begin
            rama_wr <= 0;
            set_addr_state <= 0;
        end
        endcase
    end
    else if (inc_addr) begin
        rama_wr <= 0;
        set_addr_state <= 1;
        if (increment_neg) begin
            tmp_addr <= address - increment;
        end begin
            tmp_addr <= address + increment;
        end
    end
    else begin
        rama_wr <= 0;
        set_addr_state <= 0;
    end
end