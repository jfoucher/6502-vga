// DOES NOT WORK

reg [2:0] input_state;
reg [12:0] tmp_addr;
reg [7:0] tmp_data;

`define INPUT_STATE_INIT 3'd0
`define INPUT_STATE_DATA_OUT 3'd1
`define INPUT_STATE_ADDR 3'd2
`define INPUT_STATE_DATA 3'd3
`define INPUT_STATE_INCREMENT 3'd4
`define INPUT_STATE_INCREMENT_END 3'd5
`define INPUT_STATE_WAIT 3'd6
`define INPUT_STATE_SAVE 3'd7

wire enw = write & enable;

always @(posedge enw) begin
    //tmp_data <= tmp_data;
    case (register_address)
        `CTRL_REG:
        begin
            led <= 3'b110;
            mode <= data_port & 2'b11;
            increment_reg <= data_port[6:2];
            increment_neg <= data_port[7];
            rama_wr <= 0;
            dina_i <= 0;
        end
        `ADDR_LOW_REG:
        begin
            led <= 3'b101;
            address[4:0] <= data_port[4:0];
            rama_wr <= 0;
            dina_i <= 0;
        end
        `ADDR_HIGH_REG:
        begin
            led <= 3'b101;
            address[12:5] <= data_port;
            rama_wr <= 0;
            dina_i <= 0;
        end
        `DATA_REG:
        begin
            led <= 3'b011;
            dina_i <= data_port;
            rama_wr <= 1;
            if (increment_neg) begin
                address <= address - increment;
            end
            else begin
                address <= address + increment;
            end
        end
        default:
        begin
            led <= 3'b010;
            rama_wr <= 0;
            dina_i <= 0;
        end
    endcase

end
