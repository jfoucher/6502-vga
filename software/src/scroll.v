reg[7:0] toscroll;
reg scroll_done;
reg [8:0] line;
reg [8:0] col;
reg [12:0] addr;
reg[7:0] tmp_byte;

always @(posedge CLK_100M) begin
    if (v_scroll == 0) begin
        addr <= 0;
        line <= 0;
        col <= 0;
        scroll_done <= 0;
    end
    else begin
        //we have something in v_scroll, so start scrolling

        if (col < `HOR_CHARS) begin
            col <= col + 1;
            addr <= `CHAR_DATA_START + line * `HOR_CHARS + col;
        end
        else begin
            col <= 0;
            line <= line + 1;
        end
        if (line >= `VERT_CHARS) begin
            // we are done scrolling
            line <= 0;
            col <= 0;
            addr <= 0;
            scroll_done <= 1;
        end
    end
end