

reg [7:0] char;
reg [7:0] char_px;




wire will_change_char = hires ? ((px_cnt & 3'd7) == 3'd7) && (line_cnt <= 480) : ((px_cnt & 4'hF) == 4'hD) && (line_cnt <= 480);
wire did_change_char = hires ? (((px_cnt & 3'd7) == 0) && (line_cnt <= 480)) : (((px_cnt & 4'hF) == 0) && (line_cnt <= 480)) ;

wire did_change_char_mode_3 = ((px_cnt & 3'hF) == 0) && (line_cnt <= 480);
wire mode_3_active = line_cnt > 40 && line_cnt < 640;

wire get_fg_color = hires ? ((px_cnt == 797) && (line_cnt <= 480)) : (active_video && ((px_cnt & 4'hF) == 4'hE) && (line_cnt <= 480));
wire get_bg_color = hires ? ((px_cnt == 798) && (line_cnt <= 480)) : (active_video && ((px_cnt & 4'hF) == 4'hF) && (line_cnt <= 480));

wire will_change_line = hires ? (px_cnt == 799) && (line_cnt <= 480) : (px_cnt == 797) && (line_cnt <= 480);
wire will_change_line_fg = (px_cnt == 798) && (line_cnt <= 480);
wire will_change_line_bg = (px_cnt == 799) && (line_cnt <= 480);

wire will_change_screen = hires ? (px_cnt == 799) && (line_cnt >= 524) : (px_cnt == 797) && (line_cnt >= 524);
wire will_change_screen_fg = (px_cnt == 798) && (line_cnt >= 524);
wire will_change_screen_bg = (px_cnt == 799) && (line_cnt >= 524);




wire cur_px = hires ? ((char_px >> (px_cnt & 3'd7)) & 1) : ((char_px >> ((px_cnt >> 1) & 3'd7)) & 1);



reg [7:0] fgcolor;
reg [7:0] bgcolor;

reg [7:0] tmpfg;
reg [7:0] tmpbg;


always @(posedge CLK_100M) begin
    
    case (mode)
    `MODE_CHAR_LORES:   // color character mode 50*40 char buffer, 3 bytes per character, 40*30 chars on screen, 8bit color, fg and bg per character cell
    begin
        if (will_change_screen) begin
            adb_i <= `CHAR_DATA_START;

            char <= doutb_o;
        end
        
        else if (will_change_screen_fg) begin
            adb_i <= `CHAR_DATA_START + 1;

            tmpfg <= doutb_o;
        end
        else if (will_change_screen_bg) begin
            adb_i <=  `CHAR_DATA_START + 2;

            tmpbg <= doutb_o;
        end
        else if (will_change_line) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt+1) >> 4) * `HOR_CHARS_LORES * 3;

            char <= doutb_o;
        end
        
        else if (will_change_line_fg) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt+1) >> 4) * `HOR_CHARS_LORES * 3 + 1;

            tmpfg <= doutb_o;
        end
        
        else if (will_change_line_bg) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt+1) >> 4) * `HOR_CHARS_LORES * 3 + 2;

            tmpbg <= doutb_o;
        end
        else if (will_change_char) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt >> 4) * `HOR_CHARS_LORES * 3) + (((px_cnt + 5) >> 4) * 3); //13'h0042; //
            char <= doutb_o;
        end
        else if (get_fg_color) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt >> 4) * `HOR_CHARS_LORES * 3) + (((px_cnt + 4) >> 4) * 3) + 1; //13'h0042; //
            tmpfg <= doutb_o;
        end
        else if (get_bg_color) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt >> 4) * `HOR_CHARS_LORES * 3) + (((px_cnt + 3) >> 4) * 3) + 2; //13'h0042; //
            tmpbg <= doutb_o;
        end
        else if (did_change_char) begin
            adb_i <= `FONT_DATA_START | (char << 3) | ((line_cnt >> 1) & 3'b111);
            char_px <= doutb_o;
            bgcolor <= tmpbg;
            fgcolor <= tmpfg;

        end
    end
    `MODE_CHAR_HIRES:   // monochrome character mode, 80*60 visible.
    begin
        if (will_change_screen) begin
            adb_i <= `CHAR_DATA_START;
            char <= doutb_o;
        end
        else if (will_change_line) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt+1) >> 3) * `HOR_CHARS;
            char <= doutb_o;
            fgcolor <= tmpfg; //8'h0;
            bgcolor <= tmpbg; //8'hFF;

        end
        else if (will_change_char) begin
            adb_i <= `CHAR_DATA_START + ((line_cnt >> 3) * `HOR_CHARS + ((px_cnt+1) >> 3));
            char <= doutb_o;

        end
        else if (did_change_char) begin
            adb_i <= (`FONT_DATA_START + (char << 3) + (line_cnt & 3'd7));
            char_px <= doutb_o;

        end
        else if (get_bg_color) begin
            //char_px <= 8'd0;
            adb_i <= adb_i;//13'h17FE;
            tmpbg <= 8'h00; //doutb_o;

        end
        else if (get_fg_color) begin
            //char_px <= 8'd0;
            adb_i <= adb_i;//13'h17FF;
            tmpfg <= 8'h18; //doutb_o;

        end
        else if (do_scroll) begin
            adb_i <= addr;
        end
    end 

    `MODE_PX_LORES:   // color pixel mode, 160*100, 2 pixels per byte, 16 colors, palette of 192 colors at 0x1F40
    begin
        // TODO

    end 
    `MODE_PX_LORES:   // monochrome pixel mode, 320*200, foreground color in #$1FFE background in #$1FFF
    begin
        if (mode_3_active && did_change_char_mode_3) begin
            adb_i <= `CHAR_DATA_START + (line_cnt >> 1) * 273 + px_cnt >> 4;
            char_px <= doutb_o;
            bgcolor <= tmpbg;
            fgcolor <= tmpfg;

        end
        else if (px_cnt > 650) begin
            char_px <= 8'd0;
            adb_i <= 12'h1FFF;
            tmpbg <= doutb_o;

        end
        else if (px_cnt > 680) begin
            char_px <= 8'd0;
            adb_i <= 12'h1FFE;
            tmpfg <= doutb_o;

        end
    end
    endcase
end
