reg [4:0] input_state;
reg [12:0] tmp_addr;
reg [7:0] tmp_data;
reg [7:0] ram_byte;


`define INPUT_STATE_INIT 4'd0
`define INPUT_STATE_DATA_OUT 4'd1
`define INPUT_STATE_ADDR 4'd2
`define INPUT_STATE_DATA 4'd3
`define INPUT_STATE_INCREMENT 4'd4
`define INPUT_STATE_INCREMENT_END 4'd5
`define INPUT_STATE_WAIT 4'd6
`define INPUT_STATE_SAVE 4'd7
`define INPUT_STATE_SCROLL_UP 4'd8

always @(posedge clkout_o) begin
    if(scroll_done) begin
        v_scroll <= 0;
        v_scroll_up <= 0;
    end
    else begin
        v_scroll <= v_scroll;
        v_scroll_up <= v_scroll_up;
    end
    case (input_state)
        `INPUT_STATE_INIT:
        begin
            //led <= 3'b111;
            rama_wr <= 0;
            dina_i <= 0;
            tmp_data <= 0;
            data_inout <= 0;
            data_out <= 0;
            if (enable & write) begin
                input_state <= `INPUT_STATE_ADDR;
            end
            else if (enable & read) begin
                reg_address <= register_address;
                input_state <= `INPUT_STATE_DATA_OUT;
            end
            else begin
                input_state <= `INPUT_STATE_INIT;
            end
        end

        `INPUT_STATE_ADDR:
        begin
            //led <= 3'b101;
            rama_wr <= 0;
            dina_i <= 0;
            tmp_data <= 0;
            data_inout <= 0;
            data_out <= 0;
            input_state <= `INPUT_STATE_DATA;
            
            reg_address <= register_address;
        end

        `INPUT_STATE_DATA:
        begin
            rama_wr <= 0;
            //led <= 3'b011;
            dina_i <= 0;
            data_inout <= 0;
            data_out <= 0;
            //tmp_data <= tmp_data;
            if (enable & write) begin
                input_state <= `INPUT_STATE_DATA;
                tmp_data <= data_port;
            end
            else begin
                input_state <= `INPUT_STATE_SAVE;
            end
        end


        `INPUT_STATE_DATA_OUT:
        begin
            rama_wr <= 0;
            //led <= 3'b011;
            dina_i <= 0;
            
            //tmp_data <= tmp_data;

            case (reg_address)
                `CTRL_REG:
                begin
                    data_out[1:0] <= mode;
                    data_out[6:2] <= increment_reg;
                    data_out[7] <= increment_neg;
                    data_inout <= 1;
                    if (enable & read) begin
                        input_state <= `INPUT_STATE_DATA_OUT;
                    end
                    else begin
                        input_state <= `INPUT_STATE_INIT;
                    end
                end
                `ADDR_LOW_REG:
                begin
                    data_out[4:0] <= address[4:0];
                    data_inout <= 1;
                    if (enable & read) begin
                        input_state <= `INPUT_STATE_DATA_OUT;
                    end
                    else begin
                        input_state <= `INPUT_STATE_INIT;
                    end
                end
                `ADDR_HIGH_REG:
                begin
                    data_out <= address[12:5];
                    data_inout <= 1;
                    if (enable & read) begin
                        input_state <= `INPUT_STATE_DATA_OUT;
                    end
                    else begin
                        input_state <= `INPUT_STATE_INIT;
                    end
                end
                `DATA_REG:
                begin
                    data_out <= douta_o;
                    data_inout <= 1;
                    if (enable & read) begin
                        input_state <= `INPUT_STATE_DATA_OUT;
                    end
                    else begin
                        input_state <= `INPUT_STATE_INCREMENT;
                    end
                end
                default:
                begin
                    data_inout <= 0;
                    data_out <= 0;
                    input_state <= `INPUT_STATE_WAIT;
                end
            endcase
        end

        `INPUT_STATE_SAVE:
        begin
            // We had a write to a register.
            // The data is in tmp_data
            // the register in reg_address
            // led <= 3'b110;
            data_inout <= 0;
            data_out <= 0;
            //tmp_data <= tmp_data;
            case (reg_address)
                `CTRL_REG:
                begin
                    mode <= tmp_data & 2'b11;
                    increment_reg <= tmp_data[6:2];
                    increment_neg <= tmp_data[7];
                    rama_wr <= 0;
                    dina_i <= 0;
                    input_state <= `INPUT_STATE_WAIT;
                end
                `ADDR_LOW_REG:
                begin
                    address[4:0] <= tmp_data[4:0];
                    rama_wr <= 0;
                    dina_i <= 0;
                    input_state <= `INPUT_STATE_WAIT;
                end
                `ADDR_HIGH_REG:
                begin
                    address[12:5] <= tmp_data;
                    rama_wr <= 0;
                    dina_i <= 0;
                    input_state <= `INPUT_STATE_WAIT;
                end
                `DATA_REG:
                begin
                    dina_i <= tmp_data;
                    rama_wr <= 1;
                    input_state <= `INPUT_STATE_INCREMENT;
                end
                `VSCROLL_REG:
                begin
                    // we are asked to scroll vertically
                    // We move the memory up if positive (bit 7 is zero)
                    // or down if negative (bit 7 is one)
                    if (!do_scroll) begin
                        v_scroll <= tmp_data[6:0];
                        v_scroll_up <= ~tmp_data[7];
                        input_state <= `INPUT_STATE_WAIT;
                    end
                    else begin
                        input_state <= `INPUT_STATE_SAVE;
                    end
                    
                    rama_wr <= 0;
                end
                default:
                begin
                    rama_wr <= 0;
                    dina_i <= 0;
                    input_state <= `INPUT_STATE_WAIT;
                end
            endcase
        end
        `INPUT_STATE_INCREMENT:
        begin
            // led <= 3'b110;
            rama_wr <= 0;
            dina_i <= 0;
            tmp_data <= 0;
            //data_inout <= 0;
            //data_out <= 0;
            input_state <= `INPUT_STATE_INCREMENT_END;
            if (increment_neg) begin
                tmp_addr <= address - increment;
            end
            else begin
                tmp_addr <= address + increment;
            end
        end
        `INPUT_STATE_INCREMENT_END:
        begin
            // led <= 3'b110;
            rama_wr <= 0;
            dina_i <= 0;
            address <= tmp_addr;
            //data_inout <= 0;
            input_state <= `INPUT_STATE_INIT;
        end

        `INPUT_STATE_WAIT:
        begin
            // led <= 3'b110;
            rama_wr <= 0;
            address <= address;
            dina_i <= 0;
            //data_inout <= 0;
            //data_out <= 0;
            if (enable) begin
                input_state <= `INPUT_STATE_WAIT;
            end
            else begin
                input_state <= `INPUT_STATE_INIT;
            end
        end
        
        default:
        begin
            input_state <= `INPUT_STATE_INIT;
        end
    endcase
end
